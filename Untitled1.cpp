// Program to build a simple calculator using switch Statement
#include <iostream>
#include <string>
#include <sstream>
using namespace std;
void doicho(int &a, int &b){
	int m = a;
	a = b;
	b = m;
} 
bool isInteger( string &input) {
    istringstream stream(input);
    int value;

    if (stream >> value && stream.eof()) {
        return true;
    }

    return false;
}	
int main() {
    int lst[1000];
    int n;
    cout << "Nhap so phan tu cua day : " ;
    cin >> n;
    cout << "Nhap cac chu so trong day : "<<endl ;
    for(int i = 0; i < n ; i ++){
    	cout << "lst[" << i <<"] : " ;
    	string input;
    	cin >> input;
    	while (isInteger(input) == false){
    		cout << "Ban phai nhap so nguyen" << endl;
    		cout << "lst[" << i <<"] : " ;
			cin >> input;
		}
		int f;
		istringstream(input) >> f;
		lst[i] = f;
	}
    
	cout << "Cac phan tu cua day la : " << endl;
	for (int n = 0; n < n; ++n) {
	    cout << lst[n] << " ";
	} 
	cout<<endl;
	cout << "Ban muon lam gi ?" << endl;
	cout << "1.Co bao nhieu phan tu?"<<endl;
	cout << "2.Tong cac phan tu la?"<<endl;
	cout << "3.Doi cho 2 phan tu bat ki"<<endl;
	cout << "4.Dao nguoc day so "<<endl;
	cout << "5.Sap xep cac phan tu trong day"<<endl;
	cout << "6.Cap nhat phan tu trong day"<<endl;
	int t = 1;
	while(t!=0){
		cout << "Nhap so thu tu cua viec ban muon lam:";
		int x;
		string input;
    	cin >> input;
    	while (isInteger(input) == false){
    		cout << "Nhap sai trong cac truong hop da cho"<<endl;
    		cout << "Nhap so thu tu cua viec ban muon lam:";
    		cin >> input;
		}
		int g;
		istringstream(input) >> g;
		x = g;
		switch(x){
			case 1:
			{
				cout << "So phan tu cua day la : "<<n << endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			case 3:
			{
				int a,b;
				cout << "Nhap 2 thu tu trong day ban muon doi : ";
				cin >> a >> b;
				doicho(lst[a],lst[b]);
				cout << "Day sau khi doi la : "<<endl;
				for (int i = 0; i < n;i++){
					cout<<lst[i]<< " ";
				}
				cout << endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			case 4:
			{
				int h = n / 2;
				for(int i = 0; i < h;i++){
					doicho(lst[i],lst[n-1-i]);
				}
				for (int i = 0; i < n;i++){
					cout<<lst[i]<< " ";
				}
				cout << endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			
			case 2: 
			{
				int count = 0;
				for(int i = 0;i<n;i++){
					count += lst[i];
			    }
				cout<<"Tong cac chu so trong day la : ";
				cout<<count<<endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;	
			}
			case 5:
			{
			int minn = (lst[0]);
			
			for (int i = 0;i < n-1;i++){
				minn = lst[i];
				for (int j = i + 1;j < n; j++){
					if (minn > lst[j]){
						doicho(minn,lst[j]);
					}
				}
				lst[i] = minn;
			}
			cout << "Day so sau khi sap xep la : " << endl;
			for (int i = 0; i < n;i++){
					cout<<lst[i]<< " ";
				}
			cout << endl;
			cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			case 6:
				{   
					int y,k;
					cout << "Ban muon thay doi chu so o thu tu : ";
					cin >> y ;
					cout << "Gia tri ban muon thay doi la : ";
					cin >> k;
					lst[y] = k;
					cout << "Day so sau khi thay doi la : " << endl;
					for (int i = 0; i < n;i++){
						cout<<lst[i]<< " ";
				}
					cout << endl;
					cout << "Ban co kiem tra tiep khong ?"<<endl;
					cout << "Nhap 1:Co, 0:Khong"<<endl;
					cin >> t;
					if (t == 0){
						break;
					}
					continue;
				}	
			default:
				cout << "Nhap sai trong cac truong hop da cho"<<endl;
							
	   }
    }	
}


