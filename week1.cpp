
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include <limits>

using namespace std;
void doicho(int &a, int &b){
	int m = a;
	a = b;
	b = m;
} 
bool isInteger( string &input) {
	if (input.size() == 0) return false;
	for (int i = 0; i < input.size(); i++) {
		if (!isdigit(input[i])) {
			return false;
		}
	return true;
	
	}	
}
int main() {
    int lst[1000];
    int n;
    int k;
    int l;
    string z;
    while (true) {
        cout << "Nhap so phan tu cua day: ";
        getline(cin,z);
		
        if (z.empty()){
        	cout << "Day so khong duoc xac dinh" << endl;
		}
		else{
			
			if (isInteger(z) == false){
				cout << "Day so khong duoc xac dinh" << endl;
			}
			else {
				istringstream(z) >> l;
				if (l <= 0 or l > 100){
					cout << "Day so khong duoc xac dinh" << endl;
				}
				else{
					break;
				}
				
			}
			}
		
        
    }
	n = l;
    cout << "Nhap cac chu so trong day : "<<endl ;
    for(int i = 0; i < n ; i ++){
    	cout << "lst[" << i <<"] : " ;
    	int u;
    	while (true) {
	    	string o ;
	        getline(cin,o);
	        if (o.empty()){
	        	cout << "Nhap sai" << endl;
	        	cout << "lst[" << i <<"] : " ;
			}
			else{
				if (isInteger(o) == false){
					cout << "Nhap sai" << endl;
					cout << "lst[" << i <<"] : " ;
				}
				else {
					istringstream(o) >> u;
					break;
					}
				}
			}
		lst[i] = u;
}
    
	cout << "Cac phan tu cua day la : " << endl;
	for (int n = 0; n < n; ++n) {
	    cout << lst[n] << " ";
	} 
	cout<<endl;
	cout << "Ban muon lam gi ?" << endl;
	cout << "1.Co bao nhieu phan tu?"<<endl;
	cout << "2.Tong cac phan tu la?"<<endl;
	cout << "3.Doi cho 2 phan tu bat ki"<<endl;
	cout << "4.Dao nguoc day so "<<endl;
	cout << "5.Sap xep cac phan tu trong day"<<endl;
	cout << "6.Cap nhat phan tu trong day"<<endl;
	int t = 1;
	while(t!=0){
		int x;
		int u;
		string input;
    	while (true) {
        cout << "Nhap so thu tu cua viec ban muon lam:";
        getline(cin,input);
		
        if (input.empty()){
        	cout << "Nhap sai" << endl;
		}
		else{
			if (isInteger(input) == false){
				cout << "Nhap sai" << endl;
			}
			else {
				istringstream(input) >> u;
				if (u <= 0 or u > 100){
					cout << "Nhap sai" << endl;
				}
				else{
					break;
				}
				}
			}
    	}
		
		x = u;
		switch(x){
			case 1:
			{
				cout << "So phan tu cua day la : "<<n << endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			case 3:
			{
				int a,b;
				cout << "Nhap 2 thu tu trong day ban muon doi : ";
				cin >> a >> b;
				doicho(lst[a],lst[b]);
				cout << "Day sau khi doi la : "<<endl;
				for (int i = 0; i < n;i++){
					cout<<lst[i]<< " ";
				}
				cout << endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			case 4:
			{
				int h = n / 2;
				for(int i = 0; i < h;i++){
					doicho(lst[i],lst[n-1-i]);
				}
				for (int i = 0; i < n;i++){
					cout<<lst[i]<< " ";
				}
				cout << endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			
			case 2: 
			{
				int count = 0;
				for(int i = 0;i<n;i++){
					count += lst[i];
			    }
				cout<<"Tong cac chu so trong day la : ";
				cout<<count<<endl;
				cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;	
			}
			case 5:
			{
			int minn = (lst[0]);
			
			for (int i = 0;i < n-1;i++){
				minn = lst[i];
				for (int j = i + 1;j < n; j++){
					if (minn > lst[j]){
						doicho(minn,lst[j]);
					}
				}
				lst[i] = minn;
			}
			cout << "Day so sau khi sap xep la : " << endl;
			for (int i = 0; i < n;i++){
					cout<<lst[i]<< " ";
				}
			cout << endl;
			cout << "Ban co kiem tra tiep khong ?"<<endl;
				cout << "Nhap 1:Co, 0:Khong"<<endl;
				cin >> t;
				if (t == 0){
					break;
				}
				continue;
			}
			case 6:
				{   
					int y,k;
					cout << "Ban muon thay doi chu so o thu tu : ";
					cin >> y ;
					cout << "Gia tri ban muon thay doi la : ";
					cin >> k;
					lst[y] = k;
					cout << "Day so sau khi thay doi la : " << endl;
					for (int i = 0; i < n;i++){
						cout<<lst[i]<< " ";
				}
					cout << endl;
					cout << "Ban co kiem tra tiep khong ?"<<endl;
					cout << "Nhap 1:Co, 0:Khong"<<endl;
					cin >> t;
					if (t == 0){
						break;
					}
					continue;
				}	
//			case 'flag':
//                decode = decodeURIComponent(escape(window.atob('Q1N7V2VlazFfRmxhOV9GMHVuZH0=')));
//                term.print(decode);
//                break;
			default:
				cout << "Nhap sai trong cac truong hop da cho"<<endl;
							
	   }
    }	
}

